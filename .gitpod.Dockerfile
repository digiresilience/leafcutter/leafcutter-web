FROM gitpod/workspace-full

# set up aws
RUN rm -rf ~/.aws
RUN	mkdir ~/.aws
RUN	curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "/home/gitpod/awscliv2.zip"
RUN	unzip ~/awscliv2.zip -d ~/awscliv2
RUN	sudo ~/awscliv2/aws/install --update
RUN brew install aws-vault

RUN brew install kubectl
RUN brew install pipx
RUN pipx install aws-sso-util
RUN npm i -g prettier
RUN npm i -g npm-check-updates
