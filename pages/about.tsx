import { GetServerSideProps, GetServerSidePropsContext } from "next";
import { useTranslate } from "react-polyglot";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { Grid, Container, Box, Button } from "@mui/material";
import { Layout } from "components/Layout";
import { checkAuth } from "lib/checkAuth";
import { useAppContext } from "components/AppProvider";
import { PageHeader } from "components/PageHeader";
import { AboutFeature } from "components/AboutFeature";
import { AboutBox } from "components/AboutBox";
import AbstractDiagram from "images/abstract-diagram.png";
import AboutHeader from "images/about-header.png";
import Globe from "images/globe.png";
import Controls from "images/controls.png";
import CommunityBackground from "images/community-background.png";
import Bicycle from "images/bicycle.png";

const About = () => {
  const t = useTranslate();
  const {
    colors: { white, leafcutterElectricBlue, cdrLinkOrange },
    typography: { h1, h4, p },
  } = useAppContext();
  return (
    <Layout>
      <Head>
        <title>Digital Threat Dashboard – Leafcutter</title>
      </Head>
      <PageHeader
        backgroundColor={leafcutterElectricBlue}
        sx={{
          backgroundImage: `url(${AboutHeader.src})`,
          backgroundSize: "200px",
          backgroundPosition: "bottom right",
          backgroundRepeat: "no-repeat",
        }}
      >
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Grid item xs={9}>
            <Box component="h1" sx={h1}>
              {t("aboutLeafcutterTitle")}
            </Box>
            <Box component="h4" sx={{ ...h4, mt: 1, mb: 1 }}>
              {t("aboutLeafcutterDescription")}
            </Box>
          </Grid>
        </Grid>
      </PageHeader>
      <Container maxWidth="lg">
        <AboutFeature
          title={t("whatIsLeafcutterTitle")}
          description={t("whatIsLeafcutterDescription")}
          direction="row"
          image={AbstractDiagram}
          showBackground={false}
          textColumns={8}
        />
        <AboutFeature
          title={t("whatIsItForTitle")}
          description={t("whatIsItForDescription")}
          direction="row-reverse"
          image={Controls}
          showBackground
          textColumns={8}
        />
        <AboutFeature
          title={t("whoCanUseItTitle")}
          description={t("whoCanUseItDescription")}
          direction="row"
          image={Globe}
          showBackground
          textColumns={6}
        />
      </Container>
      <AboutBox backgroundColor={cdrLinkOrange}>
        <Box component="h4" sx={{ ...h4, mt: 0 }}>
          {t("whereDataComesFromTitle")}
        </Box>
        {t("whereDataComesFromDescription")
          .split("\n")
          .map((line: string, i: number) => (
            <Box component="p" key={i} sx={p}>
              {line}
            </Box>
          ))}
      </AboutBox>
      <AboutBox backgroundColor={leafcutterElectricBlue}>
        <Box component="h4" sx={{ ...h4, mt: 0 }}>
          {t("projectSupportTitle")}
        </Box>
        {t("projectSupportDescription")
          .split("\n")
          .map((line: string, i: number) => (
            <Box component="p" key={i} sx={p}>
              {line}
            </Box>
          ))}
      </AboutBox>
      <Box
        sx={{
          backgroundImage: `url(${CommunityBackground.src})`,
          backgroundSize: "90%",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          position: "relative",
          height: "700px",
        }}
      >
        <Box sx={{ position: "absolute", left: 0, bottom: -20, width: 300 }}>
          <Image src={Bicycle} alt="" />
        </Box>
        <Container
          maxWidth="md"
          sx={{ textAlign: "center", paddingTop: "280px" }}
        >
          <Box
            component="h4"
            sx={{ ...h4, maxWidth: 500, margin: "0 auto", mt: 3 }}
          >
            {t("interestedInLeafcutterTitle")}
          </Box>
          {t("interestedInLeafcutterDescription")
            .split("\n")
            .map((line: string, i: number) => (
              <Box
                component="p"
                key={i}
                sx={{ ...p, maxWidth: 500, margin: "0 auto" }}
              >
                {line}
              </Box>
            ))}
          <Link href="mailto:info@digiresilience.org" passHref>
            <Button
              sx={{
                fontSize: 14,
                borderRadius: 500,
                color: white,
                backgroundColor: cdrLinkOrange,
                fontWeight: "bold",
                textTransform: "uppercase",
                pl: 6,
                pr: 5,
                mt: 4,
                ":hover": {
                  backgroundColor: leafcutterElectricBlue,
                  color: white,
                  opacity: 0.8,
                },
              }}
            >
              {t("contactUs")}
            </Button>
          </Link>
        </Container>
      </Box>
    </Layout>
  );
};

export default About;

export const getServerSideProps: GetServerSideProps = async (
  context: GetServerSidePropsContext
) => checkAuth(context);
