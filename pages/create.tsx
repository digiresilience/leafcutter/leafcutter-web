import { useEffect } from "react";
import { GetServerSideProps, GetServerSidePropsContext } from "next";
import Head from "next/head";
import { useTranslate } from "react-polyglot";
import { useRouter } from "next/router";
import { Box, Grid } from "@mui/material";
import { useCookies } from "react-cookie";
import { getTemplates } from "lib/opensearch";
import { Layout } from "components/Layout";
import { checkAuth } from "lib/checkAuth";
import { useAppContext } from "components/AppProvider";
import { PageHeader } from "components/PageHeader";
import { VisualizationBuilder } from "components/VisualizationBuilder";

const Create = ({ templates }) => {
  const t = useTranslate();
  const {
    colors: { cdrLinkOrange },
    typography: { h1, h4 },
  } = useAppContext();
  const router = useRouter();
  const cookieName = "searchIntroComplete";
  const [cookies, setCookie] = useCookies([cookieName]);
  const searchIntroComplete = parseInt(cookies[cookieName], 10) || 0;

  useEffect(() => {
    if (searchIntroComplete === 0) {
      setCookie(cookieName, `${1}`, { path: "/" });
      router.push(`${router.pathname}?group=search&tooltip=1&checklist=1`);
    }
  }, [searchIntroComplete, router, setCookie]);

  return (
    <Layout>
      <Head>
        <title>Digital Threat Dashboard – Leafcutter</title>
      </Head>

      <PageHeader backgroundColor={cdrLinkOrange}>
        <Grid container direction="row" spacing={2} alignItems="center">
          {/*           <Grid item xs={2} sx={{ textAlign: "center" }}>
            <Image src={SearchCreateHeader} width={100} height={100} alt="" />
          </Grid> */}
          <Grid container direction="column" item xs={10}>
            <Grid item>
              <Box component="h1" sx={{ ...h1 }}>
                {t("searchAndCreateTitle")}
              </Box>
            </Grid>
            <Grid item>
              <Box component="h4" sx={{ ...h4, mt: 1, mb: 1 }}>
                {t("searchAndCreateSubtitle")}
              </Box>
            </Grid>
            {/*      <Grid>
              <Box component="p" sx={{ ...p }}>
                {t("searchAndCreateDescription")}
              </Box>
            </Grid> */}
          </Grid>
        </Grid>
      </PageHeader>

      <VisualizationBuilder templates={templates} />
    </Layout>
  );
};

export default Create;

export const getServerSideProps: GetServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const res: any = await checkAuth(context);

  if (res.redirect) {
    return res;
  }

  res.props.templates = await getTemplates(100);

  return res;
};
