import { GetServerSideProps, GetServerSidePropsContext } from "next";
import Head from "next/head";
import { Grid, Box } from "@mui/material";
import { useTranslate } from "react-polyglot";
import { Layout } from "components/Layout";
import { checkAuth } from "lib/checkAuth";
import { getTrends } from "lib/opensearch";
import { PageHeader } from "components/PageHeader";
import { VisualizationCard } from "components/VisualizationCard";
import { useAppContext } from "components/AppProvider";

const Trends = ({ visualizations }) => {
  const t = useTranslate();
  const {
    colors: { cdrLinkOrange },
    typography: { h1, h4, p },
  } = useAppContext();

  return (
    <Layout>
      <Head>
        <title>Digital Threat Dashboard – Leafcutter</title>
      </Head>
      <PageHeader backgroundColor={cdrLinkOrange}>
        <Grid
          container
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
        >
          {/*      <Grid item xs={3} sx={{ textAlign: "center" }}>
            <Image src={SearchCreateHeader} width={200} height={200} alt="" />
          </Grid> */}
          <Grid item container direction="column" xs={12}>
            <Grid item>
              <Box component="h1" sx={{ ...h1 }}>
                {t("trendsTitle")}
              </Box>
            </Grid>
            <Grid item>
              <Box component="h4" sx={{ ...h4, mt: 1, mb: 1 }}>
                {t("trendsSubtitle")}
              </Box>
            </Grid>
            <Grid>
              <Box component="p" sx={{ ...p }}>
                {t("trendsDescription")}
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </PageHeader>
      <Grid
        container
        direction="row"
        wrap="wrap"
        spacing={3}
        justifyContent="space-between"
      >
        {visualizations.map((visualization, index) => (
          <VisualizationCard
            key={index}
            id={visualization.id}
            title={visualization.title}
            description={visualization.description}
            url={visualization.url}
          />
        ))}
      </Grid>
    </Layout>
  );
};

export default Trends;

export const getServerSideProps: GetServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const res: any = await checkAuth(context);
  if (res.redirect) {
    return res;
  }

  res.props.visualizations = await getTrends(25);

  return res;
};
