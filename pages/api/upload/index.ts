/* eslint-disable no-restricted-syntax */
import { NextApiRequest, NextApiResponse } from "next";
import { Client } from "@opensearch-project/opensearch";
import { v4 as uuid } from "uuid";
import taxonomy from "config/taxonomy.json";
import unRegions from "config/unRegions.json";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { headers: { authorization }, body: { tickets } } = req;
  const baseURL = `https://${process.env.OPENSEARCH_URL}`;
  const client = new Client({
    node: baseURL,
    ssl: {
      rejectUnauthorized: false,
    },
    headers: {
      authorization
    }
  });

  const succeeded = [];
  const failed = [];

  for await (const ticket of tickets) {
    const { id } = ticket;
    try {
      const country = ticket.country[0] ?? "none";
      const translatedCountry = taxonomy.country[country]?.display ?? "none";
      const countryDetails = unRegions.find((c) => c.name === translatedCountry);
      const augmentedTicket = {
        ...ticket,
        region: countryDetails['sub-region']?.toLowerCase().replace(" ", "-") ?? null,
        continent: countryDetails.region?.toLowerCase().replace(" ", "-") ?? null,
      }
      const out = await client.create({
        id: uuid(),
        index: "sample_tagged_tickets",
        refresh: true,
        body: augmentedTicket,
      });
      console.log(out);
      succeeded.push(id);
    } catch (e) {
      console.log(e)
      failed.push(id);
    }
  }

  const results = { succeeded, failed };
  return res.json(results)
};

export default handler;
