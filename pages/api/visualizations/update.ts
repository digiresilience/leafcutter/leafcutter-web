import { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";
import { updateUserVisualization } from "lib/opensearch";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getToken({
        req,
        secret: process.env.NEXTAUTH_SECRET,
    });

    if (!session) {
        return res.redirect("/login");
    }

    if (req.method !== "POST") {
        return res.status(500).json({ message: "Only POST requests are allowed" });
    }

    const { id, title, description, query } = req.body;
    await updateUserVisualization({
        email: session.email,
        id,
        title,
        description,
        query
    });

    return res.json({ id })
};

export default handler;

