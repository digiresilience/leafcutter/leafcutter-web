import { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";
import { performQuery } from "lib/opensearch";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getToken({
        req,
        secret: process.env.NEXTAUTH_SECRET,
    });

    if (!session) {
        return res.redirect("/login");
    }

    const { searchQuery } = req.query;
    const rawQuery = await JSON.parse(decodeURI(searchQuery as string));
    const results = await performQuery(rawQuery, 1000);

    return res.json(results)
};

export default handler;

