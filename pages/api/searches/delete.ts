import { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";
import { getUserMetadata, saveUserMetadata } from "lib/opensearch";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getToken({
        req,
        secret: process.env.NEXTAUTH_SECRET,
    });

    if (!session) {
        return res.redirect("/login");
    }

    if (req.method !== "POST") {
        return res.status(500).json({ message: "Only POST requests are allowed" });
    }

    const { email } = session;
    const { name } = JSON.parse(req.body);
    const { savedSearches } = await getUserMetadata(email);
    const updatedSavedSearches = savedSearches.filter(search => search.name !== name);
    const result = await saveUserMetadata(email, { savedSearches: updatedSavedSearches })

    return res.json({ result })
}

export default handler;



