import { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";
import { getUserMetadata } from "lib/opensearch";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getToken({
        req,
        secret: process.env.NEXTAUTH_SECRET,
    });

    if (!session) {
        return res.redirect("/login");
    }

    if (req.method !== "GET") {
        return res.status(500).json({ message: "Only GET requests are allowed" });
    }

    const { email } = session;
    const { savedSearches } = await getUserMetadata(email);

    return res.json(savedSearches)
}

export default handler;
