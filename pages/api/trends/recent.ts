import { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";
import { getTrends } from "lib/opensearch";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getToken({
        req,
        secret: process.env.NEXTAUTH_SECRET,
    });

    if (!session) {
        return res.redirect("/login");
    }

    const results = await getTrends(5);
    return res.json(results)
};

export default handler;
