import { FC, useLayoutEffect } from "react";
import { useRouter } from "next/router";
import { Grid, CircularProgress } from "@mui/material";
import Iframe from "react-iframe";
import { useAppContext } from "components/AppProvider";

export const Setup: FC = () => {
  const {
    colors: { leafcutterElectricBlue },
  } = useAppContext();
  const router = useRouter();
  useLayoutEffect(() => {
    setTimeout(() => router.push("/"), 4000);
  }, [router]);

  return (
    <Grid
      sx={{ width: "100%", height: 700 }}
      direction="row"
      justifyContent="space-around"
      alignItems="center"
      alignContent="center"
    >
      <Grid
        item
        xs={12}
        sx={{
          width: "200px",
          height: 700,
          textAlign: "center",
          margin: "0 auto",
          pt: 30,
        }}
      >
        <Iframe url="/app/home" height="1" width="1" frameBorder={0} />
        <CircularProgress
          size={80}
          thickness={5}
          sx={{ color: leafcutterElectricBlue }}
        />
      </Grid>
    </Grid>
  );
};

export default Setup;
