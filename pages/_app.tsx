/* eslint-disable react/jsx-props-no-spreading */
import { AppProps } from "next/app";
import { SessionProvider } from "next-auth/react";
import { useRouter } from "next/router";
import Head from "next/head";
import { CssBaseline } from "@mui/material";
import { CacheProvider, EmotionCache } from "@emotion/react";
import { CookiesProvider } from "react-cookie";
import { I18n } from "react-polyglot";
import { AdapterDateFns } from "@mui/x-date-pickers-pro/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers-pro";
import { AppProvider } from "components/AppProvider";
import createEmotionCache from "lib/createEmotionCache";
import Favicon from "images/favicon.ico";
import en from "locales/en.json";
import fr from "locales/fr.json";
import "@fontsource/poppins/400.css";
import "@fontsource/poppins/700.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/700.css";
import "@fontsource/playfair-display/900.css";
import "styles/global.css";
import { LicenseInfo } from "@mui/x-data-grid-pro";

LicenseInfo.setLicenseKey(
  "fd009c623acc055adb16370731be92e4T1JERVI6NDA3NTQsRVhQSVJZPTE2ODAyNTAwMTUwMDAsS0VZVkVSU0lPTj0x"
);

const clientSideEmotionCache: any = createEmotionCache();

const messages = { en, fr };

interface LeafcutterWebProps extends AppProps {
  // eslint-disable-next-line react/require-default-props
  emotionCache?: EmotionCache;
}

const LeafcutterWeb = (props: LeafcutterWebProps) => {
  const { locale } = useRouter();
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  return (
    <>
      <Head>
        <link rel="icon" type="image/png" href={Favicon.src} />
      </Head>
      <SessionProvider session={(pageProps as any).session}>
        <CacheProvider value={emotionCache}>
          <CookiesProvider>
            <CssBaseline />
            <AppProvider>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <I18n locale={locale} messages={messages[locale]}>
                  <Component {...pageProps} />
                </I18n>
              </LocalizationProvider>
            </AppProvider>
          </CookiesProvider>
        </CacheProvider>
      </SessionProvider>
    </>
  );
};

export default LeafcutterWeb;
