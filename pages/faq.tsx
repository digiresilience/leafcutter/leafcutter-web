import Head from "next/head";
import { GetServerSideProps, GetServerSidePropsContext } from "next";
import { useTranslate } from "react-polyglot";
import { Box, Grid } from "@mui/material";
import { Layout } from "components/Layout";
import { checkAuth } from "lib/checkAuth";
import { PageHeader } from "components/PageHeader";
import { Question } from "components/Question";
import { useAppContext } from "components/AppProvider";
import FaqHeader from "images/faq-header.svg";

const FAQ = () => {
  const t = useTranslate();
  const {
    colors: { lavender },
    typography: { h1, h4, p },
  } = useAppContext();

  const questions = [
    {
      question: t("whatIsLeafcutterQuestion"),
      answer: t("whatIsLeafcutterAnswer"),
    },
    {
      question: t("whoBuiltLeafcutterQuestion"),
      answer: t("whoBuiltLeafcutterAnswer"),
    },
    {
      question: t("whoCanUseLeafcutterQuestion"),
      answer: t("whoCanUseLeafcutterAnswer"),
    },
    {
      question: t("whatCanYouDoWithLeafcutterQuestion"),
      answer: t("whatCanYouDoWithLeafcutterAnswer"),
    },
    {
      question: t("whereIsTheDataComingFromQuestion"),
      answer: t("whereIsTheDataComingFromAnswer"),
    },
    {
      question: t("whereIsTheDataStoredQuestion"),
      answer: t("whereIsTheDataStoredAnswer"),
    },
    {
      question: t("howDoWeKeepTheDataSafeQuestion"),
      answer: t("howDoWeKeepTheDataSafeAnswer"),
    },
    {
      question: t("howLongDoYouKeepTheDataQuestion"),
      answer: t("howLongDoYouKeepTheDataAnswer"),
    },
    {
      question: t("whatOrganizationsAreParticipatingQuestion"),
      answer: t("whatOrganizationsAreParticipatingAnswer"),
    },
    {
      question: t("howDidYouGetMyProfileInformationQuestion"),
      answer: t("howDidYouGetMyProfileInformationAnswer"),
    },
    {
      question: t("howCanILearnMoreAboutLeafcutterQuestion"),
      answer: t("howCanILearnMoreAboutLeafcutterAnswer"),
    },
  ];

  return (
    <Layout>
      <Head>
        <title>Digital Threat Dashboard – Leafcutter</title>
      </Head>
      <PageHeader
        backgroundColor={lavender}
        sx={{
          backgroundImage: `url(${FaqHeader.src})`,
          backgroundSize: "150px",
          backgroundPosition: "bottom right",
          backgroundRepeat: "no-repeat",
        }}
      >
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Grid item>
            <Box component="h1" sx={{ ...h1 }}>
              {t("frequentlyAskedQuestionsTitle")}
            </Box>
            <Box component="h4" sx={{ ...h4, mt: 1, mb: 1 }}>
              {t("frequentlyAskedQuestionsSubtitle")}
            </Box>
            <Box component="p" sx={{ ...p }}>
              {t("frequentlyAskedQuestionsDescription")}
            </Box>
          </Grid>
        </Grid>
      </PageHeader>
      {questions.map((q, index) => (
        <Question key={index} question={q.question} answer={q.answer} />
      ))}
    </Layout>
  );
};

export default FAQ;

export const getServerSideProps: GetServerSideProps = async (
  context: GetServerSidePropsContext
) => checkAuth(context);
