import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import { Box, Grid, Container, IconButton } from "@mui/material";
import { Apple as AppleIcon, Google as GoogleIcon } from "@mui/icons-material";
import { useTranslate } from "react-polyglot";
import { LanguageSelect } from "components/LanguageSelect";
import LeafcutterLogoLarge from "images/leafcutter-logo-large.png";
import { signIn, getSession } from "next-auth/react";
import { useAppContext } from "components/AppProvider";

const Login = ({ session }) => {
  const t = useTranslate();
  const {
    colors: { leafcutterElectricBlue, lightGray },
    typography: { h1, h4 },
  } = useAppContext();
  const buttonStyles = {
    backgroundColor: lightGray,
    borderRadius: 500,
    width: "100%",
    fontSize: "16px",
    fontWeight: "bold",
  };

  return (
    <>
      <Head>
        <title>Leafcutter: Login</title>
      </Head>
      <Grid container direction="row-reverse" sx={{ p: 3 }}>
        <Grid item>
          <LanguageSelect />
        </Grid>
      </Grid>
      <Container maxWidth="md" sx={{ mt: 3, mb: 20 }}>
        <Grid container spacing={2} direction="column" alignItems="center">
          <Grid item>
            <Box sx={{ maxWidth: 200 }}>
              <Image src={LeafcutterLogoLarge} alt="" objectFit="fill" />
            </Box>
          </Grid>
          <Grid item sx={{ textAlign: "center" }}>
            <Box component="h1" sx={{ ...h1, color: leafcutterElectricBlue }}>
              {t("welcomeToLeafcutter")}
            </Box>
            <Box component="h4" sx={{ ...h4, mt: 1 }}>
              {t("welcomeToLeafcutterDescription")}
            </Box>
          </Grid>
          <Grid item>
            {!session ? (
              <Grid
                container
                spacing={3}
                direction="column"
                alignItems="center"
                sx={{ width: 450, mt: 1 }}
              >
                <Grid item sx={{ width: "100%" }}>
                  <IconButton
                    sx={buttonStyles}
                    onClick={() =>
                      signIn("google", {
                        callbackUrl: `${window.location.origin}/setup`,
                      })
                    }
                  >
                    <GoogleIcon sx={{ mr: 1 }} />
                    {`${t("signInWith")} Google`}
                  </IconButton>
                </Grid>
                <Grid item sx={{ width: "100%" }}>
                  <IconButton
                    sx={buttonStyles}
                    onClick={() =>
                      signIn("apple", {
                        callbackUrl: `${window.location.origin}/setup`,
                      })
                    }
                  >
                    <AppleIcon sx={{ mr: 1 }} />
                    {`${t("signInWith")} Apple`}
                  </IconButton>
                </Grid>
                <Grid item sx={{ mt: 2 }}>
                  <Box>
                    {t("dontHaveAccount")}{" "}
                    <Link href="mailto:info@digiresilience.org">
                      {t("requestAccessHere")}
                    </Link>
                  </Box>
                </Grid>
              </Grid>
            ) : null}
            {session ? (
              <>
                <Box component="h4" sx={h4}>
                  {`${t("welcome")}, ${
                    session.user.name ?? session.user.email
                  }.`}
                </Box>
                <Link href="/">{t("goHome")}</Link>
              </>
            ) : null}
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default Login;

export async function getServerSideProps(context) {
  const session = (await getSession(context)) ?? null;

  return {
    props: { session },
  };
}
