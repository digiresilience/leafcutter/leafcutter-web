import { FC, useState, useEffect } from "react";
import { Box, Grid, TextField, Select, MenuItem } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers-pro";
import { useTranslate } from "react-polyglot";
import { useAppContext } from "./AppProvider";

interface QueryDateRangeSelectorProps {}

export const QueryDateRangeSelector: FC<QueryDateRangeSelectorProps> = () => {
  const t = useTranslate();
  const [relativeDate, setRelativeDate] = useState("");
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const { updateQuery, query } = useAppContext();
  useEffect(() => {
    setStartDate(query.startDate.values[0] ?? null);
    setEndDate(query.endDate.values[0] ?? null);
    setRelativeDate(query.relativeDate.values[0] ?? "");
  }, [query, setStartDate, setEndDate, setRelativeDate]);

  return (
    <Box sx={{ height: 305, width: "100%", pt: 2 }}>
      <Grid container direction="column">
        <Grid item xs={12} sx={{ mb: 2 }}>
          <Select
            fullWidth
            size="small"
            placeholder={t("relativeDate")}
            value={relativeDate}
            onChange={(event: any) => {
              setStartDate(null);
              setEndDate(null);
              setRelativeDate(event.target.value);
              updateQuery({
                startDate: { values: [] },
              });
              updateQuery({
                endDate: { values: [] },
              });
              updateQuery({
                relativeDate: { values: [event.target.value] },
              });
            }}
          >
            <MenuItem value={7}>{t("last7Days")}</MenuItem>
            <MenuItem value={30}>{t("last30Days")}</MenuItem>
            <MenuItem value={90}>{t("last3Months")}</MenuItem>
            <MenuItem value={180}>{t("last6Months")}</MenuItem>
            <MenuItem value={365}>{t("lastYear")}</MenuItem>
            <MenuItem value={730}>{t("last2Years")}</MenuItem>
          </Select>
        </Grid>
        <Grid item sx={{ textAlign: "center", mb: 2 }}>
          – or –
        </Grid>
        <Grid item xs={12}>
          <DatePicker
            label={t("startDate")}
            value={startDate}
            onChange={(date) => {
              setStartDate(date);
              updateQuery({
                startDate: { values: [date] },
              });
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                sx={{
                  width: "100%",
                  color: "black",
                  "& .MuiOutlinedInput-root": {
                    borderBottomLeftRadius: 0,
                    borderBottomRightRadius: 0,
                  },
                }}
                size="small"
              />
            )}
          />
        </Grid>
        <Grid item xs={12}>
          <DatePicker
            label={t("endDate")}
            value={endDate}
            onChange={(date) => {
              setEndDate(date);
              updateQuery({
                endDate: { values: [date] },
              });
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                sx={{
                  backgroundColor: "white",
                  mt: "-1px",
                  width: "100%",
                  color: "black",
                  "& .MuiOutlinedInput-root": {
                    borderTop: 0,
                    borderTopLeftRadius: 0,
                    borderTopRightRadius: 0,
                  },
                }}
                size="small"
              />
            )}
          />
        </Grid>
      </Grid>
    </Box>
  );
};
