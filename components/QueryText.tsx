import { FC, useState, useEffect } from "react";
import { Box, Grid } from "@mui/material";
import { useTranslate } from "react-polyglot";
import taxonomy from "config/taxonomy.json";
import { colors } from "styles/theme";
import { useAppContext } from "./AppProvider";

export const QueryText: FC = () => {
  const t = useTranslate();
  const {
    typography: { h6 },
    query: q,
  } = useAppContext();

  const displayNames = {
    incidentType: t("incidentType"),
    startDate: t("startDate"),
    endDate: t("endDate"),
    relativeDate: t("relativeDate"),
    targetedGroup: t("targetedGroup"),
    platform: t("platform"),
    device: t("device"),
    service: t("service"),
    maker: t("maker"),
    country: t("country"),
    subregion: t("subregion"),
    continent: t("continent"),
  };

  const createClause = (query: any, key: string) => {
    const { values, queryType } = query[key];
    const color =
      queryType === "include"
        ? colors.leafcutterElectricBlue
        : colors.warningPink;

    if (values.length > 0) {
      return `where <span  style="color: ${color};"><strong>${
        displayNames[key]
      }</strong> ${
        queryType === "include" ? ` ${t("is")} ` : ` ${t("isNot")} `
      } ${values
        .map(
          (value: string) => `<em>${taxonomy[key]?.[value]?.display ?? ""}</em>`
        )
        .join(` ${t("or")} `)}</span>`;
    }

    return null;
  };
  const createDateClause = (query: any, key: string) => {
    const { values } = query[key];
    const color = colors.leafcutterElectricBlue;
    if (values.length > 0) {
      const range = key === "startDate" ? t("onOrAfter") : t("onOrBefore");
      return `${t("where")} <span style="color: ${color};"><strong>${
        displayNames[key]
      }</strong> is ${range} <em>${values[0]?.toLocaleDateString()}</em></span>`;
    }
    return null;
  };
  const createRelativeDateClause = (query: any, key: string) => {
    const { values } = query[key];
    const color = colors.leafcutterElectricBlue;

    if (query[key].values.length > 0) {
      const range = t("onOrAfter");
      return `${t("where")} <span style="color: ${color};"><strong>${
        displayNames[key]
      }</strong> is ${range} <em>${values[0]} days ago</em></span>`;
    }
    return null;
  };

  const [queryText, setQueryText] = useState(t("findAllIncidents"));
  useEffect(() => {
    const generateQueryText = (query: any) => {
      const incidentClause = createClause(query, "incidentType");
      const startDateClause = createDateClause(query, "startDate");
      const endDateClause = createDateClause(query, "endDate");
      const relativeDateClause = createRelativeDateClause(
        query,
        "relativeDate"
      );
      const targetedGroupClause = createClause(query, "targetedGroup");
      const platformClause = createClause(query, "platform");
      const deviceClause = createClause(query, "device");
      const serviceClause = createClause(query, "service");
      const makerClause = createClause(query, "maker");
      const countryClause = createClause(query, "country");
      const subregionClause = createClause(query, "subregion");
      const continentClause = createClause(query, "continent");
      const joinedClauses = [
        incidentClause,
        startDateClause,
        endDateClause,
        relativeDateClause,
        targetedGroupClause,
        platformClause,
        deviceClause,
        serviceClause,
        makerClause,
        countryClause,
        subregionClause,
        continentClause,
      ]
        .filter((clause) => clause !== null)
        .join(" and ");

      return `${t("findAllIncidents")} ${joinedClauses}`;
    };
    const text = generateQueryText(q);
    setQueryText(text);
  }, [q]);

  return (
    <Grid container direction="column">
      <Grid item>
        <Box sx={h6} dangerouslySetInnerHTML={{ __html: queryText }} />
      </Grid>
    </Grid>
  );
};
