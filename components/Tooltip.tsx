/* eslint-disable react/require-default-props */
import { FC } from "react";
import { useRouter } from "next/router";
import {
  Box,
  Grid,
  Tooltip as MUITooltip,
  Button,
  IconButton,
} from "@mui/material";
import { Close as CloseIcon } from "@mui/icons-material";
import { useTranslate } from "react-polyglot";
import { useAppContext } from "./AppProvider";

interface TooltipProps {
  title: string;
  description: string;
  placement: any;
  tooltipID: string;
  nextURL?: string;
  previousURL?: string;
  children: any;
}

export const Tooltip: FC<TooltipProps> = ({
  title,
  description,
  placement,
  tooltipID,
  children,
  previousURL = null,
  nextURL = null,
  // eslint-disable-next-line arrow-body-style
}) => {
  const t = useTranslate();
  const {
    typography: { p, small },
    colors: { white, leafcutterElectricBlue, almostBlack },
  } = useAppContext();
  const router = useRouter();
  const activeTooltip = router.query.tooltip?.toString();
  const open = activeTooltip === tooltipID;
  const showNavigation = true;

  return (
    <MUITooltip
      open={open}
      title={
        <Grid container direction="column">
          <Grid item container direction="row-reverse">
            <Grid item>
              <IconButton onClick={() => router.push(router.pathname)}>
                <CloseIcon
                  sx={{
                    color: leafcutterElectricBlue,
                    fontSize: "14px",
                    mt: 1,
                  }}
                />
              </IconButton>
            </Grid>
          </Grid>
          <Grid item>
            <Box sx={{ p: "12px", pt: 0, mb: "6px" }}>
              <Box sx={{ ...p, fontWeight: "bold" }}>
                <Grid container direction="row" alignItems="center">
                  <Grid item>{title}</Grid>
                </Grid>
              </Box>
              <Box sx={{ ...small, mt: 1, color: almostBlack }}>
                {description}
              </Box>
            </Box>
          </Grid>
          {showNavigation ? (
            <Grid
              item
              container
              direction="row"
              justifyContent="space-between"
              alignItems="center"
              sx={{ p: "12px" }}
            >
              <Grid item>
                {previousURL ? (
                  <Button
                    sx={{
                      ...small,
                      borderRadius: 500,
                      border: `1px solid ${leafcutterElectricBlue}`,
                      p: "2px 8px",
                      color: leafcutterElectricBlue,
                      textTransform: "none",
                    }}
                    onClick={() => router.push(previousURL)}
                  >
                    {t("previous")}
                  </Button>
                ) : null}
              </Grid>
              <Grid item>
                {nextURL ? (
                  <Button
                    sx={{
                      ...small,
                      borderRadius: 500,
                      border: `1px solid ${leafcutterElectricBlue}`,
                      p: "2px 8px",
                      color: leafcutterElectricBlue,
                      textTransform: "none",
                    }}
                    onClick={() => router.push(nextURL)}
                  >
                    {t("next")}
                  </Button>
                ) : (
                  <Button
                    sx={{
                      ...small,
                      borderRadius: 500,
                      border: `1px solid ${leafcutterElectricBlue}`,
                      p: "2px 8px",
                      color: leafcutterElectricBlue,
                      textTransform: "none",
                    }}
                    onClick={() => router.push(router.pathname)}
                  >
                    {t("done")}
                  </Button>
                )}
              </Grid>
            </Grid>
          ) : null}
        </Grid>
      }
      arrow
      placement={placement}
      sx={{ opacity: 0.9 }}
      componentsProps={{
        tooltip: {
          sx: {
            opacity: 1.0,
            backgroundColor: white,
            color: leafcutterElectricBlue,
            boxShadow: "0px 6px 20px rgba(0,0,0,0.25)",
          },
        },
        arrow: {
          sx: { opacity: 1.0, fontSize: "22px", color: white },
        },
      }}
    >
      {children}
    </MUITooltip>
  );
};
