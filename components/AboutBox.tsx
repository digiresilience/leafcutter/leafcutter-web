import { FC } from "react";
import { Box } from "@mui/material";
import { useAppContext } from "./AppProvider";

interface AboutBoxProps {
  backgroundColor: string;
}

export const AboutBox: FC<AboutBoxProps> = ({ backgroundColor, children }: any) => {
  const {
    colors: { white },
  } = useAppContext();

  return (
    <Box
      sx={{
        width: "100%",
        backgroundColor,
        color: white,
        p: 4,
        borderRadius: "10px",
        mt: "66px",
        mb: "22px",
        textAlign: "center",
      }}
    >
      {children}
    </Box>
  );
};
