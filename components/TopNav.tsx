import { FC } from "react";
import Link from "next/link";
import Image from "next/image";
import { AppBar, Grid, Box } from "@mui/material";
import { useTranslate } from "react-polyglot";
import LeafcutterLogo from "images/leafcutter-logo.png";
import { AccountButton } from "components/AccountButton";
import { HelpButton } from "components/HelpButton";
import { Tooltip } from "components/Tooltip";
import { useAppContext } from "./AppProvider";
// import { LanguageSelect } from "./LanguageSelect";

export const TopNav: FC = () => {
  const t = useTranslate();
  const {
    colors: { white, leafcutterElectricBlue, cdrLinkOrange },
    typography: { h5, h6 },
  } = useAppContext();

  return (
    <AppBar
      position="fixed"
      elevation={1}
      sx={{
        backgroundColor: white,
        marginBottom: 180,
        opacity: 0.95,
        pt: 2,
        pb: 2,
        pr: 3,
        pl: 6,
        backdropFilter: "blur(10px)",
      }}
    >
      <Grid
        container
        justifyContent="space-between"
        alignItems="center"
        alignContent="center"
        direction="row"
        wrap="nowrap"
        spacing={4}
      >
        <Link href="/" passHref>
          <Grid
            item
            container
            direction="row"
            justifyContent="flex-start"
            spacing={1}
            wrap="nowrap"
            sx={{ cursor: "pointer" }}
          >
            <Grid item sx={{ pr: 1 }}>
              <Image src={LeafcutterLogo} alt="" width={56} height={52} />
            </Grid>
            <Grid item container direction="column" alignContent="flex-start">
              <Grid item>
                <Box
                  sx={{
                    ...h5,
                    color: leafcutterElectricBlue,
                    p: 0,
                    m: 0,
                    pt: 1,
                    textAlign: "left",
                  }}
                >
                  Leafcutter
                </Box>
              </Grid>
              <Grid item>
                <Box
                  sx={{
                    ...h6,
                    m: 0,
                    p: 0,
                    color: cdrLinkOrange,
                    textAlign: "left",
                  }}
                >
                  A Project of Center for Digital Resilience
                </Box>
              </Grid>
            </Grid>
          </Grid>
        </Link>
        <Grid item>
          <HelpButton />
        </Grid>

        {/*       <Tooltip
          title={t("languageOptionsCardTitle")}
          description={t("languageOptionsCardDescription")}
          emoji="✍️"
          tooltipID="language"
          placement="bottom"
        >
          <Grid item>
            <LanguageSelect />
          </Grid>
        </Tooltip> */}
        <Tooltip
          title={t("profileSettingsCardTitle")}
          description={t("profileSettingsCardDescription")}
          tooltipID="profile"
          placement="bottom"
          previousURL="/?tooltip=recentUpdates"
          nextURL="/create?tooltip=searchCreate"
        >
          <Grid item>
            <AccountButton />
          </Grid>
        </Tooltip>
      </Grid>
    </AppBar>
  );
};
