import { FC, useState } from "react";
import { useRouter } from "next/router";
import { Button } from "@mui/material";
import { QuestionMark as QuestionMarkIcon } from "@mui/icons-material";
import { useAppContext } from "./AppProvider";

export const HelpButton: FC = () => {
  const router = useRouter();
  const [helpActive, setHelpActive] = useState(false);
  const {
    colors: { leafcutterElectricBlue },
  } = useAppContext();
  const onClick = () => {
    if (helpActive) {
      router.push(router.pathname);
    } else {
      router.push("/?tooltip=welcome");
    }
    setHelpActive(!helpActive);
  };

  return (
    <Button
      color="primary"
      onClick={onClick}
      sx={{
        backgroundColor: leafcutterElectricBlue,
        width: "40px",
        height: "40px",
        minWidth: "40px",
        p: 0,
        borderRadius: "500px",
        ":hover": {
          backgroundColor: leafcutterElectricBlue,
          opacity: 0.8,
        },
      }}
    >
      <QuestionMarkIcon width="30px" height="30px" htmlColor="white" />
    </Button>
  );
};
