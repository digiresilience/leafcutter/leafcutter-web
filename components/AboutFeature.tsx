import { FC } from "react";
import Image from "next/image";
import { Grid, Box, GridSize } from "@mui/material";
import AboutDots from "images/about-dots.png";
import { useAppContext } from "./AppProvider";

interface AboutFeatureProps {
  title: string;
  description: string;
  direction: "row" | "row-reverse";
  image: any;
  showBackground: boolean;
  textColumns: number;
}

export const AboutFeature: FC<AboutFeatureProps> = ({
  title,
  description,
  direction,
  image,
  showBackground,
  textColumns,
}) => {
  const {
    typography: { h2, p },
  } = useAppContext();

  return (
    <Box
      sx={{
        p: "20px",
        mt: "40px",
        backgroundImage: showBackground ? `url(${AboutDots.src})` : "",
        backgroundSize: "200px 200px",
        backgroundPosition: direction === "row" ? "20% 50%" : "80% 50%",
        backgroundRepeat: "no-repeat",
      }}
    >
      <Grid
        direction={direction}
        container
        spacing={5}
        alignContent="flex-start"
      >
        <Grid item xs={textColumns as GridSize}>
          <Box component="h2" sx={h2}>
            {title}
          </Box>
          <Box component="p" sx={p}>
            {description}
          </Box>
        </Grid>
        <Grid
          item
          xs={(12 - textColumns) as GridSize}
          container
          direction={direction}
        >
          <Box sx={{ width: "150px", mt: "-20px" }}>
            <Image src={image} alt="" objectFit="contain" />
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};
