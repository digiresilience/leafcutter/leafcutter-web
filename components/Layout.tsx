import { Grid, Container } from "@mui/material";
import CookieConsent from "react-cookie-consent";
import { useCookies } from "react-cookie";
import { TopNav } from "./TopNav";
import { Sidebar } from "./Sidebar";
import { GettingStartedDialog } from "./GettingStartedDialog";
import { useAppContext } from "./AppProvider";
// import { Footer } from "./Footer";

export const Layout = ({ children }) => {
  const [cookies, setCookie] = useCookies(["cookieConsent"]);
  const consentGranted = cookies.cookieConsent === "true";
  const {
    colors: {
      white,
      almostBlack,
      leafcutterElectricBlue,
      cdrLinkOrange,
      helpYellow,
    },
  } = useAppContext();

  return (
    <>
      <Grid container direction="column">
        <Grid item>
          <TopNav />
        </Grid>
        <Sidebar open />
        <Grid item sx={{ mt: "100px", ml: "310px" }}>
          <Container sx={{ padding: 2 }}>{children}</Container>
        </Grid>
        <Grid item>{/*          <Footer /> */}</Grid>
      </Grid>
      {!consentGranted ? (
        <CookieConsent
          style={{
            zIndex: 1500,
            backgroundColor: helpYellow,
            color: almostBlack,
            borderTop: `1px solid ${leafcutterElectricBlue}`,
          }}
          onAccept={() => setCookie("cookieConsent", "true", { path: "/" })}
          buttonStyle={{
            borderRadius: 500,
            backgroundColor: cdrLinkOrange,
            color: white,
            textTransform: "uppercase",
            padding: "10px 20px",
            fontWeight: "bold",
            fontSize: 14,
          }}
        >
          Leafcutter uses cookies for core funtionality.
        </CookieConsent>
      ) : null}
      <GettingStartedDialog />
    </>
  );
};
