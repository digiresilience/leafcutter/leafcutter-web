import { FC, useState } from "react";
import {
  Box,
  Grid,
  Dialog,
  DialogActions,
  Button,
  DialogContent,
} from "@mui/material";
import {
  PrivacyTip as PrivacyTipIcon,
  DateRange as DateRangeIcon,
  PhoneIphone as PhoneIphoneIcon,
  Map as MapIcon,
  Group as GroupIcon,
} from "@mui/icons-material";
import { useTranslate } from "react-polyglot";
import taxonomy from "config/taxonomy.json";
import { QueryBuilderSection } from "./QueryBuilderSection";
import { QueryListSelector } from "./QueryListSelector";
import { QueryDateRangeSelector } from "./QueryDateRangeSelector";
import { useAppContext } from "./AppProvider";
import { Tooltip } from "./Tooltip";

interface QueryBuilderProps { }

export const QueryBuilder: FC<QueryBuilderProps> = () => {
  const t = useTranslate();
  const [dialogOpen, setDialogOpen] = useState(false);
  const {
    typography: { p },
    colors: { leafcutterElectricBlue, mediumGray, almostBlack },
  } = useAppContext();

  const openAdvancedOptions = () => {
    setDialogOpen(false);
    window.open(`/app/visualize`, "_ blank");
  };

  return (
    <Box sx={{ mb: 6 }}>
      <Grid container direction="row" spacing={2}>
        <Tooltip
          title={t("categoriesCardTitle")}
          description={t("categoriesCardDescription")}
          tooltipID="categories"
          placement="left"
          previousURL="/create?tooltip=searchCreate"
          nextURL="/create?tooltip=dateRange"
        >
          <Box sx={{ width: 0 }} />
        </Tooltip>
        <QueryBuilderSection
          width={4}
          name={t("incidentType")}
          keyName="incidentType"
          Image={PrivacyTipIcon}
          showQueryType
          tooltipTitle={t("incidentTypeCardTitle")}
          tooltipDescription={t("incidentTypeCardDescription")}
        >
          <Grid container>
            <QueryListSelector
              title={t("type")}
              keyName="incidentType"
              values={taxonomy.incidentType}
              width={12}
            />
          </Grid>
        </QueryBuilderSection>
        <Tooltip
          title={t("dateRangeCardTitle")}
          description={t("dateRangeCardDescription")}
          tooltipID="dateRange"
          placement="top"
          previousURL="/create?tooltip=categories"
          nextURL="/create?tooltip=subcategories"
        >
          <Box sx={{ width: 0 }} />
        </Tooltip>
        <QueryBuilderSection
          width={4}
          name={t("date")}
          keyName="date"
          Image={DateRangeIcon}
          tooltipTitle={t("dateRangeCardTitle")}
          tooltipDescription={t("dateRangeCardDescription")}
        >
          <QueryDateRangeSelector />
        </QueryBuilderSection>
        <QueryBuilderSection
          width={4}
          name={t("targetedGroup")}
          keyName="targetedGroup"
          Image={GroupIcon}
          showQueryType
          tooltipTitle={t("targetedGroupCardTitle")}
          tooltipDescription={t("targetedGroupCardDescription")}
        >
          <Grid container>
            <QueryListSelector
              title={t("group")}
              keyName="targetedGroup"
              values={taxonomy.targetedGroup}
              width={12}
            />
          </Grid>
        </QueryBuilderSection>
        <Tooltip
          title={t("subcategoriesCardTitle")}
          description={t("subcategoriesCardDescription")}
          tooltipID="subcategories"
          placement="top"
          previousURL="/create?tooltip=dateRange"
          nextURL="/create?tooltip=advancedOptions"
        >
          <Box sx={{ width: 0 }} />
        </Tooltip>
        <QueryBuilderSection
          width={12}
          name={t("impactedTechnology")}
          keyName="impactedTechnology"
          Image={PhoneIphoneIcon}
          showQueryType
          tooltipTitle={t("impactedTechnologyCardTitle")}
          tooltipDescription={t("impactedTechnologyCardDescription")}
        >
          <Grid container spacing={2}>
            <QueryListSelector
              title={t("platform")}
              keyName="platform"
              values={taxonomy.platform}
              width={3}
            />
            <QueryListSelector
              title={t("device")}
              keyName="device"
              values={taxonomy.device}
              width={3}
            />
            <QueryListSelector
              title={t("service")}
              keyName="service"
              values={taxonomy.service}
              width={3}
            />
            <QueryListSelector
              title={t("maker")}
              keyName="maker"
              values={taxonomy.maker}
              width={3}
            />
          </Grid>
        </QueryBuilderSection>
        <QueryBuilderSection
          width={12}
          name={t("region")}
          keyName="subregion"
          Image={MapIcon}
          showQueryType={false}
          tooltipTitle={t("regionCardTitle")}
          tooltipDescription={t("regionCardDescription")}
        >
          <Grid container spacing={2}>
            <QueryListSelector
              title={t("continent")}
              keyName="continent"
              values={taxonomy.continent}
              width={4}
            />
            <QueryListSelector
              title={t("country")}
              keyName="country"
              values={taxonomy.country}
              width={4}
            />
            <QueryListSelector
              title={t("subregion")}
              keyName="subregion"
              values={taxonomy.subregion}
              width={4}
            />
          </Grid>
        </QueryBuilderSection>
        <Grid item xs={12}>
          <Tooltip
            title={t("advancedOptionsCardTitle")}
            description={t("advancedOptionsCardDescription")}
            tooltipID="advancedOptions"
            placement="top"
            previousURL="/create?tooltip=subcategories"
            nextURL="/create?tooltip=queryResults"
          >
            <Button
              sx={{
                ...p,
                color: leafcutterElectricBlue,
                textDecoration: "underline",
                textTransform: "none",
                fontWeight: "bold",
              }}
              onClick={() => setDialogOpen(true)}
            >
              {`+ ${t("advancedOptions")}`}
            </Button>
          </Tooltip>
        </Grid>
      </Grid>
      <Dialog open={dialogOpen}>
        <DialogContent sx={{ maxWidth: 350 }}>
          {t("fullInterfaceWillOpen")}
        </DialogContent>
        <DialogActions>
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            wrap="nowrap"
            sx={{ pl: 2, pr: 2, pt: 1, pb: 1 }}
          >
            <Grid item>
              <Button
                sx={{
                  backgroundColor: mediumGray,
                  color: almostBlack,
                }}
                variant="contained"
                size="small"
                onClick={() => setDialogOpen(false)}
              >
                {t("cancel")}
              </Button>
            </Grid>
            <Grid item>
              <Button
                sx={{ backgroundColor: leafcutterElectricBlue }}
                variant="contained"
                size="small"
                onClick={openAdvancedOptions}
              >
                {t("open")}
              </Button>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>
    </Box>
  );
};
