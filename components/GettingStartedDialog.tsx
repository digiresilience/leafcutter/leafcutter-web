import { FC, useState } from "react";
import { Dialog, Box, Grid, Checkbox, IconButton } from "@mui/material";
import { Close as CloseIcon } from "@mui/icons-material";
import { useRouter } from "next/router";
import { useTranslate } from "react-polyglot";
import { useAppContext } from "./AppProvider";

const CheckboxItem = ({ title, description, checked, onChange }) => {
  const {
    typography: { p, small },
  } = useAppContext();

  return (
    <Grid item container spacing={0}>
      <Grid
        item
        container
        spacing={0}
        sx={{ backgroundColor: "white" }}
        wrap="nowrap"
      >
        <Grid item xs={1}>
          <Checkbox checked={checked} onChange={onChange} sx={{ mt: "-8px" }} />
        </Grid>
        <Grid
          item
          container
          direction="column"
          spacing={0}
          xs={11}
          sx={{ pl: 2 }}
        >
          <Grid item>
            <Box sx={{ ...p, fontWeight: "bold" }}>{title}</Box>
          </Grid>
          <Grid item>
            <Box sx={small}>{description}</Box>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export const GettingStartedDialog: FC = () => {
  const {
    colors: { almostBlack },
    typography: { h4 },
  } = useAppContext();
  const t = useTranslate();
  const [completedItems, setCompletedItems] = useState([]);
  const router = useRouter();
  const open = router.query.tooltip?.toString() === "checklist";
  const toggleCompletedItem = (item) => {
    if (completedItems.includes(item)) {
      setCompletedItems(completedItems.filter((i) => i !== item));
    } else {
      setCompletedItems([...completedItems, item]);
    }
  };

  return (
    <Dialog
      open={open}
      maxWidth="xs"
      PaperProps={{
        sx: { position: "absolute", bottom: 8, right: 8, borderRadius: 3 },
      }}
      hideBackdrop
      disableEnforceFocus
      disableAutoFocus
      onBackdropClick={undefined}
    >
      <Grid container direction="column" spacing={2} sx={{ p: 3 }}>
        <Grid item>
          <Grid container direction="row" justifyContent="space-between">
            <Grid item>
              <Box sx={{ ...h4, mb: 3 }}>{t("getStartedChecklist")}</Box>
            </Grid>
            <Grid item>
              <IconButton onClick={() => router.push(router.pathname)}>
                <CloseIcon sx={{ color: almostBlack, fontSize: "18px" }} />
              </IconButton>
            </Grid>
          </Grid>
          <Grid container direction="column" spacing={2}>
            <CheckboxItem
              title={t("searchTitle")}
              description={t("searchDescription")}
              checked={completedItems.includes("search")}
              onChange={() => toggleCompletedItem("search")}
            />
            <CheckboxItem
              title={t("createVisualizationTitle")}
              description={t("createVisualizationDescription")}
              checked={completedItems.includes("create")}
              onChange={() => toggleCompletedItem("create")}
            />
            <CheckboxItem
              title={t("saveTitle")}
              description={t("saveDescription")}
              checked={completedItems.includes("save")}
              onChange={() => toggleCompletedItem("save")}
            />
            <CheckboxItem
              title={t("exportTitle")}
              description={t("exportDescription")}
              checked={completedItems.includes("export")}
              onChange={() => toggleCompletedItem("export")}
            />
            <CheckboxItem
              title={t("shareTitle")}
              description={t("shareDescription")}
              checked={completedItems.includes("share")}
              onChange={() => toggleCompletedItem("share")}
            />
          </Grid>
        </Grid>
      </Grid>
    </Dialog>
  );
};
