import { FC } from "react";
import { Box, Grid } from "@mui/material";
import { DataGridPro } from "@mui/x-data-grid-pro";
import { useTranslate } from "react-polyglot";

interface RawDataViewerProps {
  rows: any[];
  height: number;
}

export const RawDataViewer: FC<RawDataViewerProps> = ({ rows, height }) => {
  const t = useTranslate();
  const columns = [
    {
      field: "date",
      headerName: t("date"),
      editable: false,
      flex: 0.7,
      valueFormatter: ({ value }) => new Date(value).toLocaleDateString(),
    },
    {
      field: "incident",
      headerName: t("incident"),
      editable: false,
      flex: 1,
    },
    {
      field: "technology",
      headerName: t("technology"),
      editable: false,
      flex: 0.8,
    },
    {
      field: "targeted_group",
      headerName: t("targetedGroup"),
      editable: false,
      flex: 1.3,
    },
    {
      field: "country",
      headerName: t("country"),
      editable: false,
      flex: 1,
    },
    {
      field: "region",
      headerName: t("subregion"),
      editable: false,
      flex: 1,
    },
    {
      field: "continent",
      headerName: t("continent"),
      editable: false,
      flex: 1,
    },
  ];

  return (
    <Grid item xs={12}>
      <Box sx={{ width: "100%", height }} onClick={(e: any) => e.stopPropagation()}>
        <Grid container direction="column" spacing={2}>
          <Grid item>
            <DataGridPro
              sx={{ width: "100%", height }}
              rows={rows}
              columns={columns}
              density="compact"
              pageSize={100}
              disableSelectionOnClick
              hideFooter
              disableColumnMenu
              scrollbarSize={10}
              disableVirtualization
            />
          </Grid>
        </Grid>
      </Box>
    </Grid >
  );
};
