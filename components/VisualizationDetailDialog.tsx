import { FC, useState } from "react";
// import Link from "next/link";
import {
  Grid,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  TextField,
} from "@mui/material";
import { useTranslate } from "react-polyglot";
import { useAppContext } from "components/AppProvider";
import { VisualizationDetail } from "./VisualizationDetail";

interface VisualizationDetailDialogProps {
  id: string;
  title: string;
  description: string;
  url: string;
  closeDialog: any;
  editing: boolean;
}

export const VisualizationDetailDialog: FC<VisualizationDetailDialogProps> = ({
  id,
  title,
  description,
  url,
  closeDialog,
  editing,
}) => {
  const t = useTranslate();
  const [editedTitle, setEditedTitle] = useState(title);
  const [editedDescription, setEditedDescription] = useState(description);
  const {
    colors: { leafcutterElectricBlue, leafcutterLightBlue, white, almostBlack },
    query,
  } = useAppContext();

  const deleteAndClose = async () => {
    await fetch(`/api/visualizations/delete`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id }),
    });

    closeDialog();
  };

  const saveAndClose = async () => {
    const updateParams = {
      id,
      title: editedTitle,
      description: editedDescription,
      query,
    };
    await fetch(`/api/visualizations/update`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updateParams),
    });

    closeDialog();
  };

  const buttonStyles = {
    fontSize: 14,
    borderRadius: 500,
    color: white,
    backgroundColor: leafcutterElectricBlue,
    fontWeight: "bold",
    textTransform: "uppercase",
    pl: 3,
    pr: 3,
    ":hover": {
      backgroundColor: leafcutterLightBlue,
      color: almostBlack,
      opacity: 0.8,
    },
  };

  return (
    <Dialog open maxWidth="xl">
      <DialogContent sx={{ minWidth: 800 }}>
        {editing && (
          <Grid direction="column" container rowGap={2} sx={{ mb: 3 }}>
            <Grid item>
              <TextField
                value={editedTitle}
                onChange={(e) => setEditedTitle(e.target.value)}
                label={t("title")}
                size="small"
                fullWidth
              />
            </Grid>
            <Grid>
              <TextField
                value={editedDescription}
                onChange={(e) => setEditedDescription(e.target.value)}
                label={t("description")}
                size="small"
                fullWidth
              />
            </Grid>
          </Grid>
        )}
        <VisualizationDetail
          id={id}
          title={title}
          description={description}
          url={url}
          editing={editing}
        />
      </DialogContent>
      <DialogActions sx={{ p: 2.5, pt: 0 }}>
        <Grid container direction="row-reverse" justifyContent="space-between">
          {!editing && (
            <Grid item>
              <Button sx={buttonStyles} onClick={closeDialog} size="small">
                {t("done")}
              </Button>
            </Grid>
          )}
          {!editing && (
            <Grid item>
              <Button sx={buttonStyles} onClick={deleteAndClose} size="small">
                {t("delete")}
              </Button>
            </Grid>
          )}
          {editing && (
            <Grid item>
              <Button sx={buttonStyles} onClick={saveAndClose} size="small">
                {t("save")}
              </Button>
            </Grid>
          )}
          {editing && (
            <Grid item>
              <Button sx={buttonStyles} onClick={deleteAndClose} size="small">
                {t("cancel")}
              </Button>
            </Grid>
          )}
        </Grid>
      </DialogActions>
    </Dialog>
  );
};
