/* eslint-disable react/require-default-props */
import { FC } from "react";
import { Box } from "@mui/material";
import { useAppContext } from "./AppProvider";

interface PageHeaderProps {
  backgroundColor: string;
  sx?: any;
}

export const PageHeader: FC<PageHeaderProps> = ({
  backgroundColor,
  sx = {},
  children,
}: any) => {
  const {
    colors: { white },
  } = useAppContext();

  return (
    <Box
      sx={{
        width: "100%",
        backgroundColor,
        color: white,
        p: 3,
        borderRadius: "10px",
        mb: "22px",
        minHeight: "100px",
        zIndex: 1000,
        position: "relative",
        ...sx,
      }}
    >
      {children}
    </Box>
  );
};
