import { FC } from "react";
import { Container, Grid, Box, Button } from "@mui/material";
import { useTranslate } from "react-polyglot";
import Image from "next/image";
import Link from "next/link";
import leafcutterLogo from "images/leafcutter-logo.png";
import footerLogo from "images/footer-logo.png";
import twitterLogo from "images/twitter-logo.png";
import gitlabLogo from "images/gitlab-logo.png";
import { useAppContext } from "./AppProvider";

export const Footer: FC = () => {
  const t = useTranslate();
  const {
    colors: { white, leafcutterElectricBlue },
    typography: { bodySmall },
  } = useAppContext();
  const smallLinkStyles: any = {
    ...bodySmall,
    color: white,
    textTransform: "none",
  };

  return (
    <Box
      sx={{
        backgroundColor: leafcutterElectricBlue,
        backgroundImage: `url(${footerLogo})`,
        backgroundBlendMode: "overlay",
        backgroundPosition: "bottom left",
        backgroundRepeat: "no-repeat",
        backgroundSize: "30%",
        marginTop: "40px",
        marginLeft: "300px",
      }}
    >
      <Container sx={{ pt: 4, pb: 4 }}>
        <Grid
          container
          direction="row"
          wrap="nowrap"
          justifyContent="space-between"
        >
          <Grid
            item
            container
            direction="row"
            wrap="nowrap"
            sx={{ maxHeight: "auto" }}
          >
            <Grid item sx={{ width: 50, ml: 2, mr: 4 }}>
              <Image src={leafcutterLogo} alt="CDR logo" />
            </Grid>
          </Grid>
          <Grid item sx={{ color: "white" }}>
            {t("contactUs")}
          </Grid>
        </Grid>
      </Container>

      <Box sx={{ backgroundColor: leafcutterElectricBlue }}>
        <Container>
          <Grid
            item
            container
            direction="row"
            justifyContent="space-between"
            wrap="nowrap"
            alignItems="center"
          >
            <Grid
              item
              container
              direction="row"
              spacing={1}
              alignItems="center"
            >
              <Grid item>
                <Box component="p" sx={{ ...bodySmall, color: white }}>
                  ©️ {t("copyright")}
                </Box>
              </Grid>
              <Grid item>
                <Link href="/about/privacy" passHref>
                  <Button variant="text" sx={smallLinkStyles}>
                    {t("privacyPolicy")}
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Link href="/about/code-practice" passHref>
                  <Button variant="text" sx={smallLinkStyles}>
                    {t("codeOfPractice")}
                  </Button>
                </Link>
              </Grid>
            </Grid>
            <Grid item sx={{ width: 40, p: 1, pl: 0 }}>
              <a href="https://gitlab.com/digiresilience">
                <Image src={gitlabLogo} alt="Gitlab logo" />
              </a>
            </Grid>
            <Grid item sx={{ width: 40, p: 1, pr: 0 }}>
              <a href="https://twitter.com/cdr_tech">
                <Image src={twitterLogo} alt="Twitter logo" />
              </a>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </Box>
  );
};
