import { FC, useState } from "react";
import ReactMarkdown from "react-markdown";
import {
  Grid,
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@mui/material";
import {
  ChevronRight as ChevronRightIcon,
  ExpandMore as ExpandMoreIcon,
  Circle as CircleIcon,
} from "@mui/icons-material";
import { useAppContext } from "./AppProvider";

interface QuestionProps {
  question: string;
  answer: string;
}

export const Question: FC<QuestionProps> = ({ question, answer }) => {
  const [expanded, setExpanded] = useState(false);
  const {
    colors: { lavender, darkLavender },
    typography: { h5, p },
  } = useAppContext();

  return (
    <Accordion
      expanded={expanded}
      onChange={() => setExpanded(!expanded)}
      elevation={0}
      sx={{ "::before": { display: "none" } }}
    >
      <AccordionSummary>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          sx={{ maxWidth: 500 }}
        >
          <Box component="h5" sx={h5}>
            <CircleIcon
              sx={{
                fontSize: 14,
                color: expanded ? darkLavender : lavender,
                mr: 1,
                mb: "-2px",
              }}
            />
            {question}
          </Box>
          {expanded ? (
            <ExpandMoreIcon
              htmlColor={lavender}
              fontSize="medium"
              sx={{ mt: "2px" }}
            />
          ) : (
            <ChevronRightIcon
              htmlColor={lavender}
              fontSize="medium"
              sx={{ mt: "4px" }}
            />
          )}
        </Grid>
      </AccordionSummary>
      <AccordionDetails sx={{ border: 0 }}>
        <Box
          component="p"
          sx={{ ...p, p: 2, border: `1px solid ${lavender}`, borderRadius: 3 }}
        >
          <ReactMarkdown>{answer}</ReactMarkdown>
        </Box>
      </AccordionDetails>
    </Accordion>
  );
};
