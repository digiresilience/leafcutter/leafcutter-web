import { Box, Grid } from "@mui/material";
import { useSession } from "next-auth/react";
import { useTranslate } from "react-polyglot";
import { useAppContext } from "./AppProvider";

export const Welcome = () => {
  const t = useTranslate();
  const { data: session } = useSession();
  const {
    user: { name },
  } = session;
  const {
    colors: { white, leafcutterElectricBlue },
    typography: { h1, h4, p },
  } = useAppContext();

  return (
    <Box
      sx={{
        width: "100%",
        backgroundColor: leafcutterElectricBlue,
        color: white,
        p: 4,
        borderRadius: "10px",
        mb: "22px",
      }}
    >
      <Grid container direction="row" spacing={3}>
        {/*       <Grid
          item
          container
          xs={3}
          direction="column"
          justifyContent="flex-start"
          alignItems="center"
        >
          <img src={image} alt={name} width="150px" />
        </Grid> */}
        <Grid item xs={12}>
          <Box component="h1" sx={{ ...h1, mb: 1 }}>
            {t("dashboardTitle")}
          </Box>
          <Box component="h4" sx={{ ...h4, mt: 1, mb: 1 }}>{`${t("welcome")}, ${
            name?.split(" ")[0]
          }! 👋`}</Box>
          <Box component="p" sx={{ ...p }}>
            {t("dashboardDescription")}
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};
