import { Box, Grid, Dialog, Button } from "@mui/material";
import { useRouter } from "next/router";
// import { useSession } from "next-auth/react";
// import { useTranslate } from "react-polyglot";
import { useAppContext } from "./AppProvider";

export const WelcomeDialog = () => {
  // const t = useTranslate();
  const router = useRouter();
  // const { data: session } = useSession();
  // const { user } = session;
  const {
    colors: { white, leafcutterElectricBlue },
    typography: { h1, h6, p },
  } = useAppContext();
  const activeTooltip = router.query.tooltip?.toString();
  const open = activeTooltip === "welcome";

  return (
    <Dialog open={open} maxWidth="md" sx={{ zIndex: 2000 }}>
      <Box sx={{ p: 6, pt: 6 }}>
        <Grid container direction="column" spacing={3}>
          <Grid item container direction="row" justifyContent="center">
            <Grid
              item
              sx={{ width: 500, height: 300, backgroundColor: "black" }}
            >
              {/*      <iframe
                width="500"
                height="300"
                src="https://www.youtube-nocookie.com/embed/-iKFBXAlmEM"
                title="CDR Link intro"
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              /> */}
            </Grid>
          </Grid>
          <Grid item>
            <Box
              sx={{
                ...h1,
                color: leafcutterElectricBlue,
                textAlign: "center",
                fontSize: 32,
              }}
            >
              Welcome to Leafcutter!
            </Box>
            <Box
              sx={{ ...h6, color: leafcutterElectricBlue, pt: 1, fontSize: 16 }}
            >
              Let&apos;s get started.
            </Box>
          </Grid>
          <Grid item container spacing={3}>
            <Grid item>
              <Box sx={{ ...p, textAlign: "center" }}>
                Leafcutter is a secure platform for aggregating, displaying, and
                sharing data on digital security threats and attacks facing
                global civil society. When creating the app we had a couple of
                people in mind; Incident responders, threat analysts, security
                trainers, and security service providers.
              </Box>
            </Grid>
            <Grid item>
              <Box sx={{ ...p, textAlign: "center" }}>
                Leafcutter onboarding is meant to help you navigate, build
                queries and visualizations, and walk you through the best ways
                to know and use the Leafcutter app. Ready?
              </Box>
            </Grid>
          </Grid>
          <Grid
            item
            container
            direction="row"
            justifyContent="space-around"
            sx={{ mt: 3 }}
          >
            <Grid item>
              <Button
                sx={{
                  fontSize: 14,
                  minWidth: 300,
                  borderRadius: 500,
                  color: leafcutterElectricBlue,
                  border: `2px solid ${leafcutterElectricBlue}`,
                  fontWeight: "bold",
                  textTransform: "uppercase",
                  pl: 6,
                  pr: 5,
                  ":hover": {
                    backgroundColor: leafcutterElectricBlue,
                    color: white,
                    opacity: 0.8,
                  },
                }}
                onClick={() => {
                  router.push(`/`);
                }}
              >
                I&apos;ll explore on my own
              </Button>
            </Grid>
            <Grid item>
              <Button
                sx={{
                  fontSize: 14,
                  minWidth: 300,
                  borderRadius: 500,
                  backgroundColor: leafcutterElectricBlue,
                  color: white,
                  border: `2px solid ${leafcutterElectricBlue}`,
                  fontWeight: "bold",
                  textTransform: "uppercase",
                  pl: 6,
                  pr: 5,
                  ":hover": {
                    backgroundColor: leafcutterElectricBlue,
                    color: white,
                    opacity: 0.8,
                  },
                }}
                onClick={() => {
                  router.push(`/?tooltip=navigation`);
                }}
              >
                Start the guide
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </Dialog>
  );
};
