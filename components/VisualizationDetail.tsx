import { FC } from "react";
// import Link from "next/link";
import { Box } from "@mui/material";
import Iframe from "react-iframe";
import { useAppContext } from "components/AppProvider";

interface VisualizationDetailProps {
  id: string;
  title: string;
  description: string;
  url: string;
  editing: boolean;
}

export const VisualizationDetail: FC<VisualizationDetailProps> = ({
  id,
  title,
  description,
  url,
  editing,
}) => {
  const {
    colors: { mediumGray },
    typography: { h4, p },
  } = useAppContext();
  const finalURL = `${process.env.NEXT_PUBLIC_NEXTAUTH_URL}${url}&_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-3y%2Cto%3Anow))`;

  return (
    <Box key={id}>
      {!editing ? (
        <Box sx={{ borderBottom: `1px solid ${mediumGray}`, mb: 2 }}>
          <Box sx={{ ...h4, mt: 1, mb: 1 }}>{title}</Box>
          <Box sx={{ ...p, mt: 0, mb: 2, fontStyle: "oblique" }}>
            {description}
          </Box>
        </Box>
      ) : null}
      <Box sx={{ borderBottom: `1px solid ${mediumGray}`, pb: 3 }}>
        <Iframe url={finalURL} height="500px" width="100%" frameBorder={0} />
      </Box>
    </Box>
  );
};
