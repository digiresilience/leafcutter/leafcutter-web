import { FC } from "react";
import Image from "next/image";
import { Card, Grid } from "@mui/material";
import horizontalBar from "images/horizontal-bar.svg";
import horizontalBarStacked from "images/horizontal-bar-stacked.svg";
import verticalBar from "images/vertical-bar.svg";
import verticalBarStacked from "images/vertical-bar-stacked.svg";
import pieDonut from "images/pie-donut.svg";
import line from "images/line.svg";
import lineStacked from "images/line-stacked.svg";
import dataTable from "images/data-table.svg";
import metric from "images/metric.svg";
import tagCloud from "images/tag-cloud.svg";
import { useAppContext } from "./AppProvider";

interface VisualizationSelectCardProps {
  visualizationType: string;
  title: string;
  enabled: boolean;
  selected: boolean;
  toggleSelected: any;
}

export const VisualizationSelectCard: FC<VisualizationSelectCardProps> = ({
  visualizationType,
  title,
  enabled,
  selected,
  toggleSelected,
}) => {
  const {
    typography: { small },
    colors: {
      white,
      leafcutterElectricBlue,
      leafcutterLightBlue,
      cdrLinkOrange,
    },
  } = useAppContext();
  const images = {
    horizontalBar,
    horizontalBarStacked,
    verticalBar,
    verticalBarStacked,
    line,
    lineStacked,
    pieDonut,
    dataTable,
    metric,
    tagCloud,
    unknown: line,
  };

  let backgroundColor = leafcutterElectricBlue;
  if (!enabled) {
    backgroundColor = leafcutterLightBlue;
  } else if (selected) {
    backgroundColor = cdrLinkOrange;
  }

  return (
    <Card
      sx={{
        height: "100px",
        backgroundColor,
        borderRadius: "10px",
        padding: "10px",
        opacity: enabled ? 1 : 0.5,
        cursor: enabled ? "pointer" : "default",
        "&:hover": {
          backgroundColor: enabled ? cdrLinkOrange : white,
        },
      }}
      elevation={enabled ? 2 : 0}
      onClick={() => toggleSelected(visualizationType)}
    >
      <Grid
        direction="column"
        container
        justifyContent="space-around"
        alignContent="center"
        alignItems="center"
        wrap="nowrap"
        sx={{ height: "100%" }}
        spacing={0}
      >
        <Grid
          item
          sx={{
            ...small,
            textAlign: "center",
            color: enabled ? white : leafcutterElectricBlue,
          }}
        >
          {title}
        </Grid>
        <Grid item>
          <Image
            src={images[visualizationType]}
            alt=""
            width={35}
            height={35}
          />
        </Grid>
      </Grid>
    </Card>
  );
};
