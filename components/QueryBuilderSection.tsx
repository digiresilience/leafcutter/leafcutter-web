import { FC, useState } from "react";
import {
  Box,
  Grid,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Button,
  ButtonGroup,
  IconButton,
  Tooltip as MUITooltip,
} from "@mui/material";
import { useTranslate } from "react-polyglot";
import {
  ExpandMore as ExpandMoreIcon,
  Help as HelpIcon,
} from "@mui/icons-material";
import { useAppContext } from "./AppProvider";

interface QueryBuilderSectionProps {
  name: string;
  keyName: string;
  children: any;
  Image: any;
  width: number;
  // eslint-disable-next-line react/require-default-props
  showQueryType?: boolean;
  tooltipTitle: string;
  tooltipDescription: string;
}

const Tooltip = ({ title, description, children, open }) => {
  const {
    colors: { white, leafcutterElectricBlue, almostBlack },
    typography: { h5, small },
  } = useAppContext();

  return (
    <MUITooltip
      open={open}
      title={
        <Box sx={{ width: 300, p: 2, pt: 1 }}>
          <Grid container direction="column">
            <Grid
              item
              sx={{
                ...h5,
                textTransform: "none",
                textAlign: "left",
                fontWeight: 700,
                ml: 0,
                color: leafcutterElectricBlue,
              }}
            >
              {title}
            </Grid>
            <Grid item sx={{ ...small, color: almostBlack }}>
              {description}
            </Grid>
          </Grid>
        </Box>
      }
      arrow
      placement="top"
      componentsProps={{
        tooltip: {
          sx: {
            backgroundColor: white,
            boxShadow: "0px 6px 8px rgba(0,0,0,0.5)",
          },
        },
        arrow: {
          sx: {
            color: "white",
            fontSize: "22px",
          },
        },
      }}
    >
      {children}
    </MUITooltip>
  );
};

export const QueryBuilderSection: FC<QueryBuilderSectionProps> = ({
  name,
  keyName,
  children,
  Image,
  width,
  showQueryType = false,
  tooltipTitle,
  tooltipDescription,
}) => {
  const t = useTranslate();
  const [queryType, setQueryType] = useState("include");
  const [showTooltip, setShowTooltip] = useState(false);
  const {
    colors: { white, leafcutterElectricBlue, warningPink, almostBlack },
    typography: { h6, small },
    updateQueryType,
  } = useAppContext();
  const updateType = (type: string) => {
    setQueryType(type);
    updateQueryType({
      [keyName]: { queryType: type },
    });
  };

  const minHeight = "42px";
  const maxHeight = "42px";

  return (
    <Grid item xs={width}>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon sx={{ color: white, fontSize: 28 }} />}
          sx={{
            backgroundColor: leafcutterElectricBlue,
            height: "14px",
            minHeight,
            maxHeight,
            "&.Mui-expanded": {
              minHeight,
              maxHeight,
            },
          }}
        >
          <Grid container direction="row" alignItems="center">
            <Grid item>
              <Image
                sx={{ color: white, fontSize: 24, mr: "8px", mt: "2px" }}
                alt=""
              />
            </Grid>
            <Grid item>
              <Box sx={{ ...h6, color: white, fontWeight: "bold", mt: "-2px" }}>
                {name}
              </Box>
            </Grid>
            <Grid item>
              <Tooltip
                open={showTooltip}
                title={tooltipTitle}
                description={tooltipDescription}
              >
                <IconButton
                  onMouseEnter={() => setShowTooltip(true)}
                  onMouseLeave={() => setShowTooltip(false)}
                >
                  <HelpIcon
                    sx={{ color: white, width: "14px", mt: "-1px", ml: "-3px" }}
                  />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </AccordionSummary>
        <AccordionDetails>
          {showQueryType ? (
            <Grid
              container
              direction="row"
              spacing={1}
              sx={{ mt: 0, mb: 2 }}
              justifyContent="center"
            >
              <Grid item sx={{ mt: "-6px" }}>
                <ButtonGroup>
                  <Button
                    variant="contained"
                    size="small"
                    sx={{
                      fontSize: 10,
                      height: 20,
                      color: queryType === "include" ? white : almostBlack,
                      backgroundColor:
                        queryType === "include"
                          ? leafcutterElectricBlue
                          : white,
                      "&:hover": {
                        color: white,
                        backgroundColor: leafcutterElectricBlue,
                      },
                    }}
                    onClick={() => updateType("include")}
                  >
                    {t("include")}
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    sx={{
                      fontSize: 10,
                      height: 20,
                      color: queryType === "exclude" ? white : almostBlack,
                      backgroundColor:
                        queryType === "exclude" ? warningPink : white,
                      "&:hover": {
                        color: white,
                        backgroundColor: warningPink,
                      },
                    }}
                    onClick={() => updateType("exclude")}
                  >
                    {t("exclude")}
                  </Button>
                </ButtonGroup>
              </Grid>
              <Grid item>
                <Box sx={{ ...small, mt: "0px" }}>these items:</Box>
              </Grid>
            </Grid>
          ) : null}
          <Box>{children}</Box>
        </AccordionDetails>
      </Accordion>
    </Grid>
  );
};
