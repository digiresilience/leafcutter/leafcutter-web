import { FC, useState } from "react";
import { Card, Grid } from "@mui/material";
import {
  PrivacyTip as PrivacyTipIcon,
  PhoneIphone as PhoneIphoneIcon,
  Map as MapIcon,
  Group as GroupIcon,
  DateRange as DateRangeIcon,
  Public as PublicIcon,
} from "@mui/icons-material";
import { VisualizationDetailDialog } from "./VisualizationDetailDialog";
import { useAppContext } from "./AppProvider";

interface MetricSelectCardProps {
  visualizationID: string;
  metricType: string;
  title: string;
  description: string;
  enabled: boolean;
}

export const MetricSelectCard: FC<MetricSelectCardProps> = ({
  visualizationID,
  metricType,
  title,
  description,
  enabled,
}) => {
  const [open, setOpen] = useState(false);
  const closeDialog = () => setOpen(false);
  const [dialogParams, setDialogParams] = useState<any>({});
  const {
    typography: { small },
    colors: { white, leafcutterElectricBlue, cdrLinkOrange },
    query,
  } = useAppContext();
  /*   const images = {
    actor: PrivacyTipIcon,
    incidenttype: PrivacyTipIcon,
    channel: PrivacyTipIcon,
    date: DateRangeIcon,
    targetedgroup: GroupIcon,
    impactedtechnology: PhoneIphoneIcon,
    location: MapIcon,
  }; */

  const createAndOpen = async () => {
    const createParams = {
      visualizationID,
      title,
      description,
      query,
    };
    const result: any = await fetch(`/api/visualizations/create`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(createParams),
    });

    const { id } = await result.json();
    const params = {
      id,
      title: createParams.title,
      description: createParams.description,
      url: `/app/visualize?security_tenant=private#/edit/${id}?embed=true`,
    };
    setDialogParams(params);
    setOpen(true);
  };

  return (
    <>
      <Card
        sx={{
          height: "100px",
          backgroundColor: enabled ? leafcutterElectricBlue : white,
          borderRadius: "10px",
          padding: "10px",
          opacity: enabled ? 1 : 0.5,
          cursor: enabled ? "pointer" : "default",
          "&:hover": {
            backgroundColor: enabled ? cdrLinkOrange : white,
          },
        }}
        elevation={enabled ? 2 : 0}
        onClick={createAndOpen}
      >
        <Grid
          direction="column"
          container
          justifyContent="space-around"
          alignContent="center"
          alignItems="center"
          wrap="nowrap"
          sx={{ height: "100%" }}
          spacing={0}
        >
          <Grid
            item
            sx={{
              ...small,
              textAlign: "center",
              color: enabled ? white : leafcutterElectricBlue,
            }}
          >
            {title}
          </Grid>
          <Grid item>
            {metricType === "impactedtechnology" && (
              <PhoneIphoneIcon fontSize="large" sx={{ color: "white" }} />
            )}
            {metricType === "region" && (
              <PublicIcon fontSize="large" sx={{ color: "white" }} />
            )}
            {metricType === "continent" && (
              <PublicIcon fontSize="large" sx={{ color: "white" }} />
            )}
            {metricType === "country" && (
              <MapIcon fontSize="large" sx={{ color: "white" }} />
            )}
            {metricType === "targetedgroup" && (
              <GroupIcon fontSize="large" sx={{ color: "white" }} />
            )}
            {metricType === "incidenttype" && (
              <PrivacyTipIcon fontSize="large" sx={{ color: "white" }} />
            )}
            {metricType === "date" && (
              <DateRangeIcon fontSize="large" sx={{ color: "white" }} />
            )}
          </Grid>
        </Grid>
      </Card>
      {open ? (
        <VisualizationDetailDialog
          id={dialogParams.id}
          title={dialogParams.title}
          description={dialogParams.description}
          url={dialogParams.url}
          closeDialog={closeDialog}
          editing
        />
      ) : null}
    </>
  );
};
