import { FC, useEffect, useState } from "react";
import { useAppContext } from "./AppProvider";
import { RawDataViewer } from "./RawDataViewer";

export const LiveDataViewer: FC = () => {
  const { query, setFoundCount } = useAppContext();
  const [rows, setRows] = useState<any[]>([]);
  const searchQuery = encodeURI(JSON.stringify(query));
  useEffect(() => {
    const fetchData = async () => {
      const result = await fetch(
        `/api/visualizations/query?searchQuery=${searchQuery}`
      );
      const json = await result.json();
      setRows(json);
      setFoundCount(json?.length ?? 0);
    };
    fetchData();
  }, [searchQuery, setFoundCount]);

  return <RawDataViewer rows={rows} height={350} />;
};
