import { FC } from "react";
import DashboardMenuIcon from "images/dashboard-menu.png";
import AboutMenuIcon from "images/about-menu.png";
import TrendsMenuIcon from "images/trends-menu.png";
import SearchCreateMenuIcon from "images/search-create-menu.png";
import FAQMenuIcon from "images/faq-menu.png";
import Image from "next/image";
import {
  Box,
  Grid,
  Typography,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Drawer,
} from "@mui/material";
import Link from "next/link";
import { useRouter } from "next/router";
import { useTranslate } from "react-polyglot";
import { useAppContext } from "components/AppProvider";
import { Tooltip } from "components/Tooltip";
// import { ArrowCircleRight as ArrowCircleRightIcon } from "@mui/icons-material";

const MenuItem = ({
  name,
  href,
  selected,
  icon,
  iconSize,
}: // tooltipTitle,
// tooltipDescription,
{
  name: string;
  href: string;
  selected: boolean;
  icon: any;
  iconSize: number;
  // tooltipTitle: string;
  // tooltipDescription: string;
}) => {
  const {
    colors: { leafcutterLightBlue, black },
  } = useAppContext();

  return (
    <Link href={href} passHref>
      <ListItem
        button
        sx={{
          paddingLeft: "62px",
          backgroundColor: selected ? leafcutterLightBlue : "transparent",
          mt: "6px",
          "& :hover": { backgroundColor: leafcutterLightBlue },
        }}
      >
        <ListItemIcon
          sx={{
            color: `${black} !important`,
          }}
        >
          <Box
            sx={{
              width: iconSize,
              height: iconSize,
              ml: `${(20 - iconSize) / 2}px`,
              mr: `${(20 - iconSize) / 2}px`,
            }}
          >
            <Image src={icon} alt="" />
          </Box>

          <ListItemText
            primary={
              <Typography
                variant="body1"
                style={{
                  color: "#000 !important",
                  fontSize: 14,
                  fontFamily: "Roboto",
                  fontWeight: 400,
                  marginLeft: 10,
                  marginTop: -3,
                }}
              >
                {name}
              </Typography>
            }
          />
        </ListItemIcon>
      </ListItem>
    </Link>
  );
};

interface SidebarProps {
  open: boolean;
}

export const Sidebar: FC<SidebarProps> = ({ open }) => {
  const t = useTranslate();
  const router = useRouter();
  const section = router.pathname.split("/")[1];
  const {
    colors: { white }, //  leafcutterElectricBlue, leafcutterLightBlue,
  } = useAppContext();

  // const [recentUpdates, setRecentUpdates] = useState([]);

  /*
  useEffect(() => {
    const getRecentUpdates = async () => {
      const result = await fetch(`/api/trends/recent`);
      const json = await result.json();
      setRecentUpdates(json);
    };
    getRecentUpdates();
  }, []);
  */

  return (
    <Drawer
      sx={{ width: 300, flexShrink: 0 }}
      color="secondary"
      variant="permanent"
      anchor="left"
      open={open}
      PaperProps={{
        sx: {
          width: 300,
          border: 0,
          mt: "90px",
          mb: "200px",
        },
      }}
    >
      <Grid
        container
        direction="column"
        justifyContent="space-between"
        wrap="nowrap"
        sx={{ backgroundColor: white, height: "100%" }}
      >
        <Grid item container direction="column" sx={{ mt: "6px" }} flexGrow={1}>
          <Tooltip
            title={t("dashboardNavigationCardTitle")}
            description={t("dashboardNavigationCardDescription")}
            tooltipID="navigation"
            placement="right"
            nextURL="/?tooltip=recentUpdates"
            previousURL="/?tooltip=welcome"
          >
            <List component="nav">
              <MenuItem
                name={t("dashboardMenuItem")}
                href="/"
                icon={DashboardMenuIcon}
                iconSize={20}
                selected={section === ""}
              />
              <MenuItem
                name={t("searchAndCreateMenuItem")}
                href="/create"
                icon={SearchCreateMenuIcon}
                iconSize={20}
                selected={section === "create"}
              />
              <MenuItem
                name={t("trendsMenuItem")}
                href="/trends"
                icon={TrendsMenuIcon}
                iconSize={13}
                selected={section === "trends"}
              />
              <MenuItem
                name={t("faqMenuItem")}
                href="/faq"
                icon={FAQMenuIcon}
                iconSize={20}
                selected={section === "faq"}
              />
              <MenuItem
                name={t("aboutMenuItem")}
                href="/about"
                icon={AboutMenuIcon}
                iconSize={20}
                selected={section === "about"}
              />
            </List>
          </Tooltip>
        </Grid>
        {/*
        <Tooltip
          title={t("recentUpdatesCardTitle")}
          description={t("recentUpdatesCardDescription")}
          tooltipID="recentUpdates"
          nextURL="/?tooltip=profile"
          previousURL="/?tooltip=navigation"
          placement="right"
        >
          <Grid
            item
            sx={{
              overflow: "hidden",
              backgroundColor: leafcutterLightBlue,
              height: 350,
            }}
          >
            <Typography
              variant="body1"
              sx={{
                fontWeight: 700,
                fontSize: 14,
                backgroundColor: leafcutterElectricBlue,
                color: white,
                p: 2,
              }}
            >
              {t("recentUpdatesTitle")}
            </Typography>
            {recentUpdates.map((trend, index) => (
              <Link href={`/visualizations/${trend.id}`} passHref key={index}>
                <Box
                  key={index}
                  sx={{
                    p: 2,
                    cursor: "pointer",
                  }}
                >
                  <Typography
                    variant="body2"
                    sx={{ color: leafcutterElectricBlue, fontWeight: 700 }}
                  >
                    {trend.title}
                  </Typography>
                  <Typography
                    variant="body2"
                    sx={{ color: leafcutterElectricBlue }}
                  >
                    {trend.description}{" "}
                    <ArrowCircleRightIcon
                      sx={{ height: 16, width: 16, mb: "-4px" }}
                    />
                  </Typography>
                </Box>
              </Link>
            ))}
          </Grid>
                </Tooltip> */}
      </Grid>
    </Drawer>
  );
};
