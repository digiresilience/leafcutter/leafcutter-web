import { useRouter } from "next/router";
import { IconButton, Menu, MenuItem, Box } from "@mui/material";
import { KeyboardArrowDown as KeyboardArrowDownIcon } from "@mui/icons-material";
import {
  usePopupState,
  bindTrigger,
  bindMenu,
} from "material-ui-popup-state/hooks";
import { useAppContext } from "./AppProvider";
// import { Tooltip } from "./Tooltip";

export const LanguageSelect = () => {
  const {
    colors: { white, leafcutterElectricBlue },
  } = useAppContext();
  const router = useRouter();
  const locales = { en: "English", fr: "Français" };
  const popupState = usePopupState({ variant: "popover", popupId: "language" });

  return (
    <Box>
      <IconButton
        {...bindTrigger(popupState)}
        sx={{
          fontSize: 14,
          borderRadius: 500,
          color: white,
          backgroundColor: leafcutterElectricBlue,
          fontWeight: "bold",
          textTransform: "uppercase",
          pl: 4,
          pr: 3,
          ":hover": {
            backgroundColor: leafcutterElectricBlue,
            opacity: 0.8,
          },
        }}
      >
        {locales[router.locale] ?? locales.en}
        <KeyboardArrowDownIcon />
      </IconButton>
      <Menu {...bindMenu(popupState)}>
        {Object.keys(locales).map((locale) => (
          <MenuItem
            key={locale}
            onClick={() => {
              router.push(router.route, router.route, { locale });
              popupState.close();
            }}
          >
            <Box sx={{ width: 130 }}>{locales[locale]}</Box>
          </MenuItem>
        ))}
      </Menu>
    </Box>
  );
};

/*  <Tooltip
      title={t("languageTooltipTitle")}
      description={t("languageTooltipDescription")}
      color="#fff"
      backgroundColor="#a5a6f6"
      placement="top"
  > </Tooltip> */
