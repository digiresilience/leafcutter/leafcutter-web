import { FC } from "react";
import Image from "next/image";
import { signOut } from "next-auth/react";
import { Button, Box, Menu, MenuItem } from "@mui/material";
import { useTranslate } from "react-polyglot";
import UserIcon from "images/user-icon.png";
import {
  usePopupState,
  bindTrigger,
  bindMenu,
} from "material-ui-popup-state/hooks";
import { useAppContext } from "./AppProvider";

export const AccountButton: FC = () => {
  const t = useTranslate();
  const {
    colors: { leafcutterElectricBlue },
  } = useAppContext();
  const popupState = usePopupState({ variant: "popover", popupId: "account" });

  return (
    <>
      <Button
        {...bindTrigger(popupState)}
        color="secondary"
        sx={{
          backgroundColor: leafcutterElectricBlue,
          width: "40px",
          height: "40px",
          minWidth: "40px",
          p: 0,
          borderRadius: "500px",
          ":hover": {
            backgroundColor: leafcutterElectricBlue,
            opacity: 0.8,
          },
        }}
      >
        <Image src={UserIcon} alt="account" width="30px" height="30px" />
      </Button>
      <Menu {...bindMenu(popupState)}>
        <MenuItem
          onClick={() => {
            popupState.close();
            signOut({ callbackUrl: "/" });
          }}
        >
          <Box sx={{ width: "100%" }}>{t("signOut")}</Box>
        </MenuItem>
      </Menu>
    </>
  );
};
