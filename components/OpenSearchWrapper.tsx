import { FC } from "react";
import Iframe from "react-iframe";
import { Box } from "@mui/material";

interface OpenSearchWrapperProps {
  url: string;
  marginTop: string;
}

export const OpenSearchWrapper: FC<OpenSearchWrapperProps> = ({
  url,
  marginTop,
}) => (
  <Box sx={{ position: "relative", marginTop: "-100px" }}>
    <Box
      sx={{
        width: "100%",
        height: "100px",
        marginTop: "-20px",
        backgroundColor: "white",
        zIndex: 100,
        position: "relative",
      }}
    />
    <Box
      sx={{
        marginTop,
        zIndex: 1,
        position: "relative",
        height: "100vh",
      }}
    >
      <Iframe
        id="opensearch"
        url={`${process.env.NEXT_PUBLIC_NEXTAUTH_URL}${url}&_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-3y%2Cto%3Anow))`}
        width="100%"
        height="100%"
        frameBorder={0}
      />
    </Box>
  </Box>
);
