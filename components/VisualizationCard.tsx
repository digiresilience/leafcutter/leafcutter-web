import { FC, useState } from "react";
import { Grid, Card, Box } from "@mui/material";
import Iframe from "react-iframe";
import { useAppContext } from "components/AppProvider";
import { VisualizationDetailDialog } from "components/VisualizationDetailDialog";

interface VisualizationCardProps {
  id: string;
  title: string;
  description: string;
  url: string;
}

export const VisualizationCard: FC<VisualizationCardProps> = ({
  id,
  title,
  description,
  url,
}) => {
  const [open, setOpen] = useState(false);
  const closeDialog = () => setOpen(false);
  const {
    typography: { h4, p },
    colors: { leafcutterLightBlue, leafcutterElectricBlue },
  } = useAppContext();
  const finalURL = `${process.env.NEXT_PUBLIC_NEXTAUTH_URL}${url}&_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-3y%2Cto%3Anow))`;

  return (
    <>
      <Grid item xs={6}>
        <Card
          elevation={0}
          sx={{
            border: `1px solid ${leafcutterElectricBlue}`,
            borderRadius: "10px",
            backgroundColor: leafcutterLightBlue,
            p: 2,
            cursor: "pointer",
          }}
          onClick={() => setOpen(true)}
        >
          <Box
            sx={{
              backgroundColor: leafcutterLightBlue,
              pointerEvents: "none",
              borderRadius: "8px",
              overflow: "hidden",
              p: 1,
            }}
          >
            <Iframe url={finalURL} height="300" width="100%" frameBorder={0} />
          </Box>
          <Box component="h4" sx={{ ...h4, mt: 2, mb: 2 }}>
            {title}
          </Box>
          <Box component="p" sx={{ ...p, mt: 2, mb: 2 }}>
            {description}
          </Box>
        </Card>
      </Grid>
      {open ? (
        <VisualizationDetailDialog
          id={id}
          title={title}
          description={description}
          url={url}
          closeDialog={closeDialog}
          editing={false}
        />
      ) : null}
    </>
  );
};
