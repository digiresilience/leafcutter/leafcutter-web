FROM public.ecr.aws/bitnami/node:18-debian-11 as builder
LABEL maintainer="Darren Clarke <darren@redaranj.com>"
RUN useradd -u 1001 -g root node
RUN mkdir /home/node
RUN chown -R 1001:root /home/node
ARG APP_DIR=/opt/leafcutter
RUN mkdir -p ${APP_DIR}/
COPY . ${APP_DIR}/
RUN chown -R 1001:root ${APP_DIR}/
USER node
WORKDIR ${APP_DIR}
ENV NEXT_PUBLIC_NEXTAUTH_URL=https://lc.digiresilience.org
RUN npm install
RUN npm run build

FROM public.ecr.aws/bitnami/node:18-debian-11
ARG BUILD_DATE
ARG VERSION
LABEL maintainer="Darren Clarke <darren@redaranj.com>"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.version=$VERSION
ARG APP_DIR=/opt/leafcutter
ENV APP_DIR ${APP_DIR}

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y --no-install-recommends \
    dumb-init
RUN useradd -u 1001 -g root node
RUN mkdir -p /home/node && chown -R 1001:root /home/node
RUN mkdir -p ${APP_DIR}
RUN chown -R 1001:root ${APP_DIR}/
COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
USER node
WORKDIR ${APP_DIR}
COPY --from=builder ${APP_DIR}/node_modules ./node_modules
COPY --from=builder ${APP_DIR}/.next ./.next
COPY --from=builder ${APP_DIR}/next.config.js ./next.config.js

EXPOSE 3000
ENV PORT 3000
ENV NODE_ENV production
COPY package.json ./
ENTRYPOINT ["/docker-entrypoint.sh"]
