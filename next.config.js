module.exports = {
  i18n: {
    locales: ["en", "fr"],
    defaultLocale: "en",
  },

  rewrites: async () => ({
    fallback: [
      {
        source: "/:path*",
        destination: "/api/proxy/:path*",
      },
    ],
  }),
};
