#!/bin/bash

set -e

cd ${APP_DIR}
echo "starting admin app"
exec dumb-init npm run start
